public class CriadorDocumentacao {

	public static void main(String[] args) {
		// HTML
		FabricaDeDocumento fabrica = new FabricaHtml();
		Documento doc = fabrica.getDocumento("<h1>Teste</h1>", "h1.html");
		System.out.println("Conteudo: \n"+doc.getConteudo()+"\n documento: "+doc.getNome());
		System.out.println();

		//MarkDown
		fabrica = new FabricaMarkDown();
		doc = fabrica.getDocumento("#Teste", "Test.md");
		System.out.println("Conteudo: \n"+doc.getConteudo()+"\n documento: "+doc.getNome());
		System.out.println();

		//Latex
		fabrica = new FabricaMarkDown();
		doc = fabrica.getDocumento("A ideia central do \\LaTeX\\ é distanciar o autor\n" +
				"o máximo possível da apresentação visual da informação.", "doc.latex");
		System.out.println("Conteudo: \n"+doc.getConteudo()+"\n documento: "+doc.getNome());

	}

}
