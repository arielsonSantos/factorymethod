public class Latex implements Documento {

	private String conteudo;

	private String nome;

	public Latex(String conteudo, String nome) {
		this.conteudo = conteudo;
		this.nome = nome;
	}

	@Override
	public String getConteudo() {
		return conteudo;
	}

	@Override
	public String getNome() {
		return nome;
	}
}
