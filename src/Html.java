public class Html implements Documento {

	private String conteudo;

	private String nome;

	public Html(String conteudo, String nome) {
		this.conteudo = conteudo;
		this.nome = nome;
	}

	@Override
	public String getConteudo() {
		return conteudo;
	}

	@Override
	public String getNome() {
		return nome;
	}

}
